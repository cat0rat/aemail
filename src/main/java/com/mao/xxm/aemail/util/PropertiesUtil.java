package com.mao.xxm.aemail.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {
	private static Properties properties ;
	static {
		properties = new Properties();
		try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getValue(String key){
		return (String) properties.get(key);
	}
}
