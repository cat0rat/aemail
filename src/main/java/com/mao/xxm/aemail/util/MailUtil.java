package com.mao.xxm.aemail.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 * 邮件发送工具类, 使用方式:
 * <pre>
 * MailUtil mail = new MailUtil();
 * mail.setAddress("cat0rat@163.com", MailUtil.TO);
 * mail.setFromAddress("aemail2017@163.com");
 * mail.setSMTPHost("smtp.163.com", "mail123456", "abc123456");
 * mail.setSubject("测试一下");
 * mail.setHtmlBody("猫");
 * mail.sendBatch();
 * </pre>
 * 注意： 需要mail.jar, 并且不能使用MyEclipse自带的javaee.jar(即移除掉或更换高版本)
 * @author Mao 2014-7-10 上午1:31:10
 */
public class MailUtil {
	/** 发件方式 - 普通发送 */
	final public static int TO = 0;
	/** 发件方式 - 抄送 */
	final public static int CC = 1;
	/** 发件方式 - 密件抄送 */
	final public static int BCC = 2;
	/** 邮件相关信息 - SMTP 服务器 */
	private String mailSMTPHost = null;
	/** 邮件相关信息 - 邮件用户名 */
	private String mailUser = null;
	/** 邮件相关信息 - 密码 */
	private String mailPassword = null;
	/** 邮件相关信息 - 发件人邮件地址 */
	private String mailFromAddress = null;
	/** 邮件相关信息 - 邮件主题 */
	private String mailSubject = "";
	/** 邮件相关信息 - 邮件发送地址 */
	private Address[] mailTOAddress = null;
	/** 邮件相关信息 - 邮件抄送地址 */
	private Address[] mailCCAddress = null;
	/** 邮件相关信息 - 邮件密件抄送地址 */
	private Address[] mailBCCAddress = null;
	/** 邮件相关信息 - 邮件正文(复合结构) */
	private MimeMultipart mailBody = null;

	public MailUtil() {
		mailBody = new MimeMultipart();
	}

	/**
	 * 设置 SMTP 服务器, 即发送者(邮箱服务器地址, 发送者帐号, 发送者密码) 如: <br>
	 * mail.setSMTPHost("smtp.126.com", "qf229100", "qianfux");
	 * @param SMTPHost (String) 邮件服务器名称或 IP, 如: smtp.126.com、smtp.qq.com
	 * @param user (String) 邮件用户名, "qf229100", "@"之前的内容
	 * @param pwd (String) 密码
	 */
	public void setSMTPHost(String SMTPHost, String user, String pwd) {
		this.mailSMTPHost = SMTPHost;
		this.mailUser = user;
		this.mailPassword = pwd;
	}

	/**
	 * 设置邮件发送地址, 如: "542490798@qq.com"
	 * @param strFromAddress (String) 邮件发送地址
	 */
	public void setFromAddress(String strFromAddress) {
		this.mailFromAddress = strFromAddress;
	}

	/**
	 * 设置邮件目的地址
	 * @param address (String) 邮件目的地址列表, 不同的地址可用;号分隔
	 * @param addrType (int) 邮件发送方式 (普通发送:TO 0, 抄送:CC 1, 密件抄送:BCC 2) 常量已在本类定义
	 * @throws AddressException
	 */
	public void setAddress(String address, int addrType) throws AddressException {
		String[] addrs = address.split("\\s*;\\s*");
		Address[] as = new Address[addrs.length];
		for (int i = 0; i < addrs.length; i++) {
			as[i] = new InternetAddress((String) addrs[i]);
		}
		switch (addrType) {
			case MailUtil.TO: mailTOAddress = as; break;
			case MailUtil.CC: mailCCAddress = as; break;
			case MailUtil.BCC: mailBCCAddress = as; break;
		}
	}

	/**
	 * 设置邮件主题
	 * @param subject 邮件主题
	 */
	public void setSubject(String subject) {
		this.mailSubject = subject;
	}

	/**
	 * 设置邮件文本正文
	 * @param text 邮件文本正文
	 * @throws MessagingException
	 */
	public void setTextBody(String text) throws MessagingException {
		MimeBodyPart mimebodypart = new MimeBodyPart();
		mimebodypart.setText(text, "UTF-8");
		mailBody.addBodyPart(mimebodypart);
	}

	/**
	 * 设置邮件超文本正文
	 * @param html 邮件超文本正文
	 * @throws MessagingException
	 */
	public void setHtmlBody(String html) throws MessagingException {
		MimeBodyPart mimebodypart = new MimeBodyPart();
		mimebodypart.setDataHandler(new DataHandler(html, "text/html;charset=UTF-8"));
		mailBody.addBodyPart(mimebodypart);
	}

	/**
	 * 设置邮件正文外部链接 URL, 信体中将包含链接所指向的内容
	 * @param urlAttach 邮件正文外部链接 URL
	 * @throws MessagingException
	 * @throws MalformedURLException
	 */
	public void setURLAttachment(String urlAttach) throws MessagingException, MalformedURLException {
		MimeBodyPart mimebodypart = new MimeBodyPart();
		mimebodypart.setDataHandler(new DataHandler(new URL(urlAttach)));
		mailBody.addBodyPart(mimebodypart);
	}

	/**
	 * 设置邮件附件
	 * @param fileAttach 文件的全路径
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void setFileAttachment(String fileAttach) throws MessagingException, UnsupportedEncodingException {
		File path = new File(fileAttach);
		if (!path.exists() || path.isDirectory()) {
			return;
		}
		String strFileName = path.getName();
		MimeBodyPart mimebodypart = new MimeBodyPart();
		mimebodypart.setDataHandler(new DataHandler(new FileDataSource(fileAttach)));
		mimebodypart.setFileName(MimeUtility.encodeText(strFileName));	//处理编码
		mailBody.addBodyPart(mimebodypart);
	}

	/**
	 * 邮件发送(一次发送多个地址, 优点速度快, 但是有非法邮件地址时将中断发送操作)
	 * @throws MessagingException
	 */
	public boolean sendBatch() throws MessagingException {
		//设置属性
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", this.mailSMTPHost);
		props.setProperty("mail.debug", "false");
		Session session = Session.getDefaultInstance(props);
		//设置地址
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(this.mailFromAddress));
		if (mailTOAddress != null) {
			message.addRecipients(Message.RecipientType.TO, this.mailTOAddress);
		}
		if (mailCCAddress != null) {
			message.addRecipients(Message.RecipientType.CC, this.mailCCAddress);
		}
		if (mailBCCAddress != null) {
			message.addRecipients(Message.RecipientType.BCC, this.mailBCCAddress);
		}
		//设置内容
		message.setSubject(this.mailSubject);
		message.setContent(this.mailBody);
		message.setSentDate(new Date());
		message.saveChanges();	//+
		Transport transport = session.getTransport("smtp");
		transport.connect(this.mailSMTPHost, this.mailUser, this.mailPassword);
		//Transport.send(message);	//- 这句有问题, 使用下面的方式即正确
		transport.sendMessage(message, message.getAllRecipients());	//x 这句很关键
		addrLog();
		return true;
	}
	
	/** 已发送地址信息 */
	public void addrLog(){
		System.out.println("已向下列邮箱发送了邮件");
		if (mailTOAddress != null) {
			for (int i = 0; i < mailTOAddress.length; i++) {
				System.out.println(mailTOAddress[i]);
			}
		}
		if (mailCCAddress != null) {
			for (int i = 0; i < mailTOAddress.length; i++) {
				System.out.println(mailCCAddress[i]);
			}
		}
		if (mailBCCAddress != null) {
			for (int i = 0; i < mailTOAddress.length; i++) {
				System.out.println(mailBCCAddress[i]);
			}
		}
	}

	static public void main(String str[]) throws Exception {
		int start = 50;
		for(int i = start; i < start + 1; i++){
			testName(i);
		}
	}
	
	public static void testName(int x) throws Exception {
		MailUtil mail = new MailUtil();
		mail.setAddress("qf229100@126.com", MailUtil.TO);
		mail.setFromAddress("aemail2017@163.com");
		mail.setSMTPHost("smtp.163.com", "aemail2017@163.com", "abc123456");
		mail.setSubject("您好~欢迎使用" + x + "号");
		mail.setHtmlBody("猫");
		mail.sendBatch();
	}
	
}
