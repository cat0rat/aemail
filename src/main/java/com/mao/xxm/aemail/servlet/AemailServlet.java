package com.mao.xxm.aemail.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mao.xxm.aemail.util.MailUtil;
import com.mao.xxm.aemail.util.PropertiesUtil;

public class AemailServlet extends HttpServlet {
	private static final long serialVersionUID = -2722697991936636585L;
	
	private String fromAddress = PropertiesUtil.getValue("fromAddress");
	private String fromSmtp = PropertiesUtil.getValue("fromSmtp");
	private String fromUser = PropertiesUtil.getValue("fromUser");
	private String fromPwd = PropertiesUtil.getValue("fromPwd");

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	/**
	 * localhost:31008/aemail/aemail <br>
	 * address:qf229100@126.com <br>
	 * subject:您好~欢迎使用Mao <br>
	 * htmlBody:<div style="color:red;">小懒猫</div> 
	 * <br><br>
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=utf-8");
		String retmsg = "{\"code\":\"200\",\"msg\":\"\"}";
		String errmsg = null;

		String address = request.getParameter("address");
		String subject = request.getParameter("subject");
		String htmlBody = request.getParameter("htmlBody");
		
		String country = request.getParameter("country");
		if(!isEmpty(country)){
			country = toStr(country);
			String company = toStr(request.getParameter("company"));
			String info = toStr(request.getParameter("info"));
			String remark = toStr(request.getParameter("remark"));
			
			if(isEmpty(address)) address = "info@zhongyunwangan.com";
			subject = company + "-申请使用中云网安";
			htmlBody = new StringBuilder()
				.append("国家:").append(country).append("<br>")
				.append("公司名称:").append(company).append("<br>")
				.append("联系方式:").append(info).append("<br>")
				.append("备注:").append(remark).append("<br>")
				.toString();
		}else{
			if(isEmpty(address)){
				errmsg = "收件人地址不能为空";
			}else if(isEmpty(subject)){
				errmsg = "主题不能不空";
			}else if(isEmpty(htmlBody)){
				errmsg = "内容不能不空";
			}
		}
		
		if(errmsg == null){
			try {
				MailUtil mail = new MailUtil();
				mail.setAddress(address, MailUtil.TO);
				mail.setFromAddress(fromAddress);
				mail.setSMTPHost(fromSmtp, fromUser != null && fromUser.length() != 0 ? fromUser : fromAddress, fromPwd);
				mail.setSubject(subject);
				mail.setHtmlBody(htmlBody);
				mail.sendBatch();
			} catch (Exception e) {
				retmsg = "{\"code\":\"500\",\"msg\":\"邮件发送失败\"}";
				e.printStackTrace();
			}
		}else{
			retmsg = "{\"code\":\"500\",\"msg\":\"" + errmsg + "\"}";
		}
		
		PrintWriter out = response.getWriter();
		out.print(retmsg);
		out.flush();
		out.close();
	}
	
	private String toStr(String str){
		return str == null ? "" : str;
	}
	
	private boolean isEmpty(String str){
		return str == null || str.length() == 0;
	}

}
