### 1 发送信息到固定邮箱
#### 请求方式
	POST(x-www-form-urlencoded)
#### URL
	/aemail/aemail
#### 输入
	String country;		// 必填, 国家
	String company;		// 必填, 公司名称
	String info;		// 必填, 联系方式
	String remark;		// 备注
	String address;		// 测试专用, 为空则发送到 info@zhongyunwangan.com
#### 示例
	country:中国
	company:小懒猫有限公司
	info:542490798@qq.com
	remark:只是一个备注
	address:709136855@qq.com
#### 输出(成功)
	{"code":"200","msg":""}
#### 输出(失败)
	{"code":"500","msg":"邮件发送失败"}


### 2 发送邮件
#### 请求方式
	POST(x-www-form-urlencoded)
#### URL
	/aemail/aemail
#### 输入
	String address;		// 必填, 收件地址
	String subject;		// 必填, 主题
	String htmlBody;	// 必填, 内容
#### 示例
	address:qf229100@126.com 
	subject:您好~欢迎使用Mao 
	htmlBody:小懒猫
#### 输出(成功)
	{"code":"200","msg":""}
#### 输出(失败)
	{"code":"500","msg":"邮件发送失败"}